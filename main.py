from tkinter import *
import time, locale, requests, Adafruit_DHT

sensor = Adafruit_DHT.DHT22
pin = 4

locale.setlocale(locale.LC_ALL, 'de_DE.utf8')

fg = "white"
bg = "#3b3b3b"

weather_api_secret = "PLEASE PUT YOUR OPENWEATHERMAP API SECRET HERE"
weather_place = "Bonn"

window = Tk()
window.title("Sensor Dashboard")
window.geometry("800x480")
window.config(bg=bg)
window.resizable(width=False, height=False)
window.attributes('-fullscreen', True)

def exit():
	window.destroy()

exitButton = Button(window, text="Exit", command=exit).place(x=750, y=0)

hum = StringVar()
temp = StringVar()
co2 = StringVar()

hum_ext = StringVar()
temp_ext = StringVar()
weather = StringVar()

dateval = StringVar()
timeval = StringVar()

font = "Arial"
fontsize = "30"

label_left = Label(window, text="Innen:", fg=fg, bg=bg, font=(font, fontsize, "bold")).place(x=20,y=20)
label_right = Label(window, text="Außen:", fg=fg, bg=bg, font=(font, fontsize, "bold")).place(x=420,y=20)

label_temp = Label(window, text="Temp.", fg=fg, bg=bg, font=(font, fontsize)).place(x=20,y=100)
label_temp_data = Label(window, textvariable=temp, fg=fg, bg=bg, font=(font, fontsize)).place(x=180,y=100)
label_temp_format = Label(window, text="°C", fg=fg, bg=bg, font=(font, fontsize)).place(x=280,y=100)

label_hum = Label(window, text="Luftf.", fg=fg, bg=bg, font=(font, fontsize)).place(x=20,y=160)
label_hum_data = Label(window, textvariable=hum, fg=fg, bg=bg, font=(font, fontsize)).place(x=180,y=160)
label_hum_format = Label(window, text="%", fg=fg, bg=bg, font=(font, fontsize)).place(x=280,y=160)

label_co2 = Label(window, text="CO₂", fg=fg, bg=bg, font=(font, fontsize)).place(x=20,y=220)
label_co2_data = Label(window, textvariable=co2, fg=fg, bg=bg, font=(font, fontsize)).place(x=180,y=220)
label_co2_format = Label(window, text="ppm", fg=fg, bg=bg, font=(font, fontsize)).place(x=280,y=220)

label_temp_ext = Label(window, text="Temp.", fg=fg, bg=bg, font=(font, fontsize)).place(x=420,y=100)
label_temp_ext_data = Label(window, textvariable=temp_ext, fg=fg, bg=bg, font=(font, fontsize)).place(x=580,y=100)
label_temp_ext_format = Label(window, text="°C", fg=fg, bg=bg, font=(font, fontsize)).place(x=680,y=100)

label_hum_ext = Label(window, text="Luftf.", fg=fg, bg=bg, font=(font, fontsize)).place(x=420,y=160)
label_hum_ext_data = Label(window, textvariable=hum_ext, fg=fg, bg=bg, font=(font, fontsize)).place(x=580,y=160)
label_hum_ext_format = Label(window, text="%", fg=fg, bg=bg, font=(font, fontsize)).place(x=680,y=160)

#label_weather = Label(window, text="Wetter", fg=fg, bg=bg, font=(font, fontsize)).place(x=420,y=220)
label_weather_data = Label(window, textvariable=weather, fg=fg, bg=bg, font=(font, "25")).place(x=420,y=220)
#label_weather_format = Label(window, text="ppm", fg=fg, bg=bg, font=(font, fontsize)).place(x=280,y=220)

#label_time = Label(window, text="Uhrzeit", fg=fg, bg=bg, font=(font, fontsize)).place(x=20,y=320)
label_time_data = Label(window, textvariable=timeval, fg=fg, bg=bg, font=(font, "60")).place(x=20,y=300)

#label_date = Label(window, text="Datum", fg=fg, bg=bg, font=(font, fontsize)).place(x=20,y=380)
label_date_data = Label(window, textvariable=dateval, fg=fg, bg=bg, font=(font, "20")).place(x=20,y=400)

temp.set("--")
hum.set("--")
co2.set("--")

temp_ext.set("--")
hum_ext.set("--")
weather.set("--")

def getWeather():
	global weather
	global temp_ext
	global hum_ext
	try:
		r = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+weather_place+",de&units=metric&lang=de&APPID="+weather_api_secret)
		res = r.json()
		#print(str(res))
		weather.set(res['weather'][0]['description'])
		temp_ext.set(res['main']['temp'])
		hum_ext.set(res['main']['humidity'])
	except Exception as e:
		weather.set("--")
		temp_ext.set("--")
		hum_ext.set("--")
	window.after(5*60*1000, getWeather)

def getHumTemp():
	global temp
	global hum
	try:
		dht_hum, dht_temp = Adafruit_DHT.read_retry(sensor, pin)
		temp.set("{:0.2f}".format(dht_temp))
		hum.set("{:0.2f}".format(dht_hum))

	except Exception as e:
		#print("Error communicating with DHT Sensor: " + str(e))
		temp.set("--")
		temp.set("--")

	window.after(20*1000, getHumTemp)

def getCO2():
	#TODO
	pass

def tick():
	global timeval
	global dateval
	timeval.set(time.strftime('%H:%M'))
	dateval.set(time.strftime('%A, %d. %B %Y'))
	window.after(200, tick)

getHumTemp()
getWeather()
getCO2()
tick()

window.mainloop()